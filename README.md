
<!-- README.md is generated from README.Rmd. Please edit that file -->

# easy16S <a href="https://easy16s.migale.inrae.fr"><img src="man/figures/logo.png" alt="easy16S website" align="right" height="139"/></a>

<!-- badges: start -->

[![Lifecycle:
stable](https://img.shields.io/badge/lifecycle-stable-green.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
[![DOI](https://joss.theoj.org/papers/10.21105/joss.06704/status.svg)](https://doi.org/10.21105/joss.06704)
[![SWH](https://archive.softwareheritage.org/badge/swh:1:dir:5d122436bf50a8cac09857ef266170585a3fddc9/)](https://archive.softwareheritage.org/swh:1:dir:5d122436bf50a8cac09857ef266170585a3fddc9;origin=https://forgemia.inra.fr/migale/easy16s)
[![HAL](https://img.shields.io/badge/-hal--04304559-1E198E?style=flat&logo=HAL)](https://hal.science/hal-04304559)

<!-- badges: end -->

> Easy16S is designed to facilitate the exploration, visualization, and
> analysis of microbiome data.

The goal of easy16S is to provide a user-friendly interactive
web-application with convenient functions and default settings to
explore, visualize and analyze metabarcoding data.

It builds upon the [phyloseq](https://joey711.github.io/phyloseq/)
package and its extensions and is tightly integrated with the
[FROGS](https://frogs.toulouse.inrae.fr/index.html) sequence processing
suite.

Easy16S can be accessed directly via an online instance:
<https://shiny.migale.inrae.fr/app/easy16S>.

## Installation

You can install the development version of easy16S like so:

``` r
# install.packages("remotes")
remotes::install_gitlab(
  repo = "migale/easy16S@main",
  host = "forgemia.inra.fr"
)
```

## Run the Shiny Application

To run the Shiny application, execute the following code in your R
environment:

``` r
easy16S::run_app()
easy16S::run_app(physeq = phyloseq.extended::food) # directly load your data
```

## Docker Integration

For each release, a [CI/CD pipeline](.gitlab-ci.yml) builds a Docker
image using a [Dockerfile](Dockerfile). This process is particularly
useful for deploying with ShinyProxy.

The Docker images are available in the [container
registry](https://forgemia.inra.fr/migale/easy16s/container_registry).

The image exposes port `3838` and launches the app using
`easy16S::run_app(options = list(launch.browser = FALSE))`

Therefore, to run the application, execute the following commands:

``` bash
docker pull registry.forgemia.inra.fr/migale/easy16s:v24.07
docker run --rm --publish 3838:3838 registry.forgemia.inra.fr/migale/easy16s:v24.07
```

The app will be accessible at <http://localhost:3838/>.

## Disclaimer

Easy16S facilitates the exploration, visualization, and analysis of
metabarcoding data with ease. However, users should be cautious about
over-interpreting the results. Proper interpretation of metagenomics
data requires a solid understanding of microbial ecology, biostatistics,
and the specific field of study. While our tool is designed to be
user-friendly, the complexity of metagenomics data analysis means that
the results can be misleading if not carefully evaluated.

We have incorporated several default settings and guardrails to guide
users and reduce the risk of misuse. However, if you lack a background
in metagenomics, it is highly advisable to collaborate with
bioinformaticians and biostatisticians to ensure robust and reliable
conclusions from your work.

## FAQ

> It is called easy”16S”. Can I use it with another marker?

Yes! Easy16S is the historical name. This tool was originally developed
for 16S amplicon data. This is why the “16S” has remained in the name.
It can also be used to analyze other metagenomics data such as ITS or
MAGs. However, care must be taken with the interpretations made, and
results must not be over-interpreted.

## Issues and Support

If you encounter any bugs or have suggestions for improvement, please
use the [issue
tracker](https://forgemia.inra.fr/migale/easy16s/-/issues).

## Cite us

If you use `easy16S` in your research or applications, please cite the
associated publication in *Journal of Open Source Software* :
[![DOI](https://joss.theoj.org/papers/10.21105/joss.06704/status.svg)](https://doi.org/10.21105/joss.06704).

> Midoux et al., (2024). Easy16S: a user-friendly Shiny web-service for
> exploration and visualization of microbiome data. Journal of Open
> Source Software, 9(103), 6704, <https://doi.org/10.21105/joss.06704>

``` bibtex
@article{easy16S,
  title = {Easy16S: a user-friendly Shiny web-service for exploration and
  visualization of microbiome data.},
  author = {Cédric Midoux and Olivier Rué and Olivier Chapleur and Ariane Bize and Valentin Loux and Mahendra Mariadassou},
  journal = {Journal of Open Source Software},
  publisher = {The Open Journal},
  year = {2024},
  volume = {9},
  number = {103},
  pages = {6704},
  doi = {10.21105/joss.06704},
  issn = {2475-9066},
  url = {https://easy16s.migale.inrae.fr},
}
```

If you used the online version, we also appreciate acknowledgment from
research teams using computing resources. Please include the following
sentence in the publications of your analyses:

> We are grateful to the INRAE MIGALE bioinformatics facility (MIGALE,
> INRAE, 2020. Migale bioinformatics Facility, doi:
> 10.15454/1.5572390655343293E12) for providing help and computing
> resources.

## Code of Conduct

Please note that the easy16S project is released with a [Contributor
Code of
Conduct](https://contributor-covenant.org/version/2/1/CODE_OF_CONDUCT.html).
By contributing to this project, you agree to abide by its terms.
