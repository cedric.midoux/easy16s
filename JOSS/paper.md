---
# Example from https://joss.readthedocs.io/en/latest/submitting.html
title: 'Easy16S: a user-friendly Shiny web-service for exploration and visualization of microbiome data.'
tags:
  - R-package
  - shiny
  - metagenomics
  - biom
  - dataviz
authors:
  - name: Cédric Midoux
    orcid: 0000-0002-7964-0929
    affiliation: "1, 2, 3"
  - name: Olivier Rué
    orcid: 0000-0001-7517-4724
    affiliation: "2, 3"
  - name: Olivier Chapleur
    orcid: 0000-0001-9460-921X
    affiliation: 1
  - name: Ariane Bize
    orcid: 0000-0003-4023-8665
    affiliation: 1
  - name: Valentin Loux
    orcid: 0000-0002-8268-915X
    affiliation: "2, 3"
  - name: Mahendra Mariadassou
    orcid: 0000-0003-2986-354X
    affiliation: "2, 3"
affiliations:
 - name: Université Paris-Saclay, INRAE, PROSE, 92761, Antony, France
   index: 1
 - name: Université Paris-Saclay, INRAE, MaIAGE, 78350, Jouy-en-Josas, France
   index: 2
 - name: Université Paris-Saclay, INRAE, BioinfOmics, MIGALE bioinformatics facility, 78350, Jouy-en-Josas, France
   index: 3
year: 2024
bibliography: paper.bib
joss-doi: 10.21105/joss.xxxxx
---

# Summary

The analysis of microbiome data has become a major asset for investigating microbial diversity and dynamics, in diverse fields, like health [@LeChatelier2013], environmental studies [@Karimi2020], food-processing [@Chaillou2014], or environmental biotechnologies [@Poirier2016]. Due to sequencing advances, microbiome studies now require exploration, analysis and interpretation of large and high-dimensional datasets. Metabarcoding approaches, in particular, are based on a two-step process. First, a bioinformatics pipeline processes raw amplicon sequencing reads into Operational Taxonomic Unit (OTU) or Amplicon Sequence Variant (ASV), generating counts and taxonomic affiliations for each of them. Second, these tables are enriched with sample metadata to investigate relevant biological questions using statistical analyses. The affordability of amplicon sequencing has led to its widespread use in microbial ecology. Therefore, there is a growing demand for user-friendly interactive tools, enabling researchers to analyze their data autonomously, alleviating the dependence on bioinformaticians, biostatisticians or the need to acquire skills in R programming.

Regarding the bioinformatics part, many solutions are available to produce count tables from reads [@Hakimzadeh2023], relying either on command-line tools provided through a Galaxy interface (ie: `QIIME` [@QIIME], `FROGS` [@FROGS]), or on R pipelines (ie: `DADA2` [@DADA2]). For the second step, several packages dedicated to the analysis and visualization of microbiomes are available, such as `phyloseq` [@phyloseq], `microbiome` [@microbiome], `metacoder` [@metacoder] and many others. Their use can however sometimes be complex. There are relatively few tools available for rapid, interactive analysis of microbiome data (`shiny-phyloseq` [@shiny-phyloseq] is no longer supported ; `ranacapa` [@ranacapa] lacks flexibility, especially during import step ; `animalcules` [@animalcules] requires local installation ; `shaman` [@shaman] is integrated into a global workflow and cannot be decoupled from the bioinformatics components).

# Statement of need

Here, we introduce `Easy16S`, an R-package and an interactive Shiny application [@shiny], aiming to facilitate exploratory microbiome data analysis, data visualization, and statistical analysis. This tool is specifically designed for biologists eager to swiftly explore their data and generate figures interactively. It is easy-to-use and especially focused on the mapping of covariates of interest to microbiome structure.

This application is built upon phyloseq-class objects powered by [@phyloseq]. These objects integrate a matrix of OTU/ASV abundances per sample, a data.frame of metadata and covariates associated to the samples, a matrix of taxonomic affiliations for each OTU/ASV, and optionally, a phylogenetic tree and reference sequences for the OTU/ASV.

# App Features

For data loading, users have three options: they can use one of the demo dataset, upload flat files to enable the application to construct a phyloseq object, or directly upload a phyloseq object.

Before analysis, uploaded data can be preprocessed by the user to help refine and clean raw data. This includes options such as sample/taxa filtering, modification of the taxonomy table, rarefaction of data and application of mathematical functions to transform the count matrix.

The various exploration and analysis sections are :

-   Key tables constituting the phyloseq object;
-   Metadata visualization using `esquisse` [@esquisse];
-   Taxonomic composition barplot;
-   Rarefaction curves;
-   Abundance heatmap;
-   Richness within a sample (α-diversity): table, scatterplot and ANOVA;
-   Dissimilarity between samples (β-diversity): table, sample heatmap, sample clustering, MultiDimensional Scaling and Multivariate ANOVA;
-   Principal Component Analysis;
-   Differential abundance analysis with `DESeq2` [@DESeq2] with sensible defaults.

Users can export preprocessed data for further analysis within R or for use in future Easy16S sessions. Additionally, tables and plots can be easily exported, enhancing the usability and accessibility of both data and results. This flexibility enables users to seamlessly integrate Easy16S with their preferred analysis tools and workflows.

# Use-case

Three major use cases have been identified for Easy16S:

-   Easy16S empowers beginner users to autonomously conduct their analyses without any technical skills.
-   More advanced users can use Easy16S to swiftly explore data, identify patterns, before developing their R code for a more in-depth analysis.
-   During training sessions, Easy16S serves as a valuable tool, allowing users to concentrate on mastering biological concepts without being encumbered by programming challenges.

Nevertheless, the simplicity of analysis should not mask the complexity of interpretation and conclusion. Some parameters and guardrails have been established to decrease the risk of misusage of statistics. However, the outcomes must be analyzed with caution and must not be over-interpreted.

![Summary in Easy16S and three examples of data visualization](screenshot/image.png)

# Acknowledgements

We are grateful to the INRAE @MIGALE bioinformatics facility (MIGALE, INRAE, 2020. Migale bioinformatics Facility, doi: 10.15454/1.5572390655343293E12) for providing help, computing and storage resources. Special thanks to Chrystelle Bureau, Patrick Dabert, and all testers for their invaluable tests and suggestions during the development of Easy16S. We also extend our thanks to the INRAE SK8 Team for their advice and recommendations throughout the Easy16S development and deployment.

# References
