test_that("nullify works", {
  expect_null(nullify(NULL))
  expect_null(nullify(""))
  expect_null(nullify("NULL"))
  expect_null(nullify(0, y = c(0)))
})

test_that("slot_phyloseq", {
  expect_true(slot_phyloseq(phyloseq.extended::food))
  expect_true(slot_phyloseq(phyloseq.extended::food, "tax_table"))
  expect_false(slot_phyloseq(phyloseq.extended::food, "refseq"))
  data("esophagus", package = "phyloseq")
  expect_true(slot_phyloseq(esophagus, "otu_table"))
  expect_false(slot_phyloseq(esophagus, "tax_table"))
})
